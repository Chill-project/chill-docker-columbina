Chill Columbina flavor
=======================

What is Chill ?
----------------

Chill is a software for social workers. It helps them by achieving two goals : 

- having all information about the person accompanyied on their eyes ;
- accelerating administrative tasks.

More information: http://chill.social. Documentation is available at http://docs.chill.social

This is the standard flavor with main bundleds. This image is used in production.

What does this container contains ?
------------------------------------

This container serve the app with php-fpm.

The app contains those modules :

- Person bundle (https://git.framasoft.org/Chill-project/Person)
- Report bundle (https://git.framasoft.org/Chill-project/Report)
- ICPC2 bundle (https://git.framasoft.org/Chill-project/icpc2)

And all the required dependencies.

Building
========

(this step is optional, you can download a builded image with `docker pull chill/columbina-php-fpm:v1.0-branch-1.0`.

```
#pull the docker image used to collect vendors
docker pull chill/ci-image
#collect the app and vendor in the src/code directory
./pre-build.sh
# build the container
docker build .
```

Running
=======

Database
--------

Start a database :

```
docker run -P --name chill_db_temp chill/database
```

Starting php-fpm
------------------

Start this container with a link to database :

- if you build on previous step with the tag `columbina-fpm`:

```
docker run --link chill_db_temp:db --name chill_fpm --env "ADMIN_PASSWORD=pass"  --env "SECRET=i_am_not_secret" columbina-fpm
```

- if you download from docker hub: 

```
docker run --link chill_db_temp:db --env "ADMIN_PASSWORD=pass"  --env "SECRET=i_am_not_secret" --name chill_fpm chill/columbina-php-fpm:v1.0-branch-1.0
```

**Notes**

In the commande above: 

- `chill_db_temp` is the database container's name. You may replace this by the name you give to the database name's container ;
- `db` is the expected database host inside the container. You can override this with adding the ENV parameter `DATABASE_HOST` (Example: `--env "DATABASE_HOST=another_host`)
- if you want the container to be destroyed on shut-down (all data will be lost), add `--rm` in the command

The container should start, run migrations files on database, dump assets, then run php-fpm and wait for connections.


Nginx
-----

Then start a nginx container to serve the content. We use the [standard nginx image](https://hub.docker.com/_/nginx/), with an adaptation of the configuration. You can download [our configuration file from the repository](https://raw.githubusercontent.com/Chill-project/docker-chill/master/standard-flavor-php-fpm/nginx.conf)

```
# download the sample configuration file
wget https://git.framasoft.org/Chill-project/chill-docker-columbina/raw/master/nginx.conf
# run the container
docker run --link chill_fpm:fpm --name nginx -p 8080:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf --volumes-from chill_fpm nginx
```

**Notes**

- in `--link` argument, `chill-fpm` match the container's name for the php-fpm code (launched the previous step)
- in `--link` argument, `fpm` match the expected container name in `nginx.conf` ([see here](https://github.com/Chill-project/docker-chill/blob/master/standard-flavor-php-fpm/nginx.conf#L45)) ;
- you can replace `--name nginx` by any name you want ;
- `-v` argument replace the nginx.conf file in the standard nginx image with the custom configuration
- `--volumes-from` import volumes from the fpm container. This is required to let nginx serve assets (javascripts, images, css, ...)
- in the command above, the exported port is `8080`. You can stick to port `80` if you prefer by removing `-p 8080:80`

Chill should be accessible on http://localhost:8080 (if you exposed port 8080).

Available options
==================

Those environnement variable are available :

- `ADMIN_PASSWORD` (required) : the admin password. 
- `SECRET` (required) : the secret used by the symfony kernel ([explanations](http://symfony.com/doc/current/reference/configuration/framework.html#secret))
- `DATABASE_HOST`: The host to connect the database. Default: `db`
- `DATABASE_NAME`: The database name. Default: `postgres`
- `DATABASE_USER`: The database username. Default: `postgres`
- `DATABASE_PASSWORD`: The database password. Default: `postgres`
- `DATABASE_PORT`: The database port. Default: `5432`
- `LOCALE`: The default locale. Default: `fr`

I want to override templates
==============================

You juste have to [add a volume](https://docs.docker.com/userguide/dockervolumes/) inside the container at `/var/www/chill/app/Resources`. Example : 

```
docker run --env "ADMIN_PASSWORD=abcde" --env "SECRET=123456" --link chill_db_temp:db --name chill_fpm -v /path/to/my/resources/Resources:/var/www/chill/app/Resources:ro chill/columbina-php-fpm:v1.0-branch-1.0
```

[Read the full symfony documentation about overriding templates](http://symfony.com/doc/current/book/templating.html#overriding-bundle-templates).


Running console commands
========================

You can run command into a running container using `docker exec`. 

```
# assuming that the container's name is 'chill_fpm':
docker exec -ti chill_fpm /usr/local/bin/php /var/www/chill/app/console
```

Stopping the containers
=======================

You can stop properly the container using 

```
# replace eventually by the name you assigned to your containers
docker stop nginx
docker stop chill_fpm
docker stop chill_db_temp
```

Removing the containers
========================

This command will destroy your containers **and removing all data from your hard disk**.

```
# replace eventually by the name you assigned to your containers
docker rm --volumes nginx
docker rm --volumes chill_fpm
docker rm --volumes chill_db_temp
```

