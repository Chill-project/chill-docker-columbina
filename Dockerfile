FROM php:5.6-fpm

MAINTAINER <julien.fastre@champs-libres.coop>

RUN apt-get update && apt-get -y install wget ca-certificates \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add -

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list

RUN apt-get update && apt-get -y install libicu52 \ 
   libicu-dev \
   g++ \
   postgresql-server-dev-9.4 
RUN docker-php-ext-install intl pdo_pgsql mbstring

#remove unused packages
RUN apt-get remove -y wget libicu-dev \
   g++ \
   && apt-get autoremove -y 

#php configuration
RUN printf "\n[Date]\ndate.timezone = Europe/Brussels\n" >> /usr/local/etc/php/php.ini

RUN printf "\nping.path = /ping\npm.status_path = /status\n" >> /usr/local/etc/php-fpm.conf 

WORKDIR /var/www/chill
COPY src/code /var/www/chill

ADD entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh

VOLUME ["/var/www/chill/web/js", "/var/www/chill/web/css", "/var/www/chill/web/bundles", "/var/www/chill/app/logs"]

CMD ["/entrypoint.sh"]

