#!/bin/bash



docker run --rm --volume `pwd`:/app -e SYMFONY_ENV=prod chill/ci-image:latest \
   echo "installing project in 'src/code' directory" \
   && composer create-project --stability=RC --no-dev --no-install \
         --no-interaction --prefer-dist chill-project/standard ./src/code ~1.0 \
   && cp composer.json  ./src/code/composer.json \
   && cp parameters.yml ./src/code/app/config/parameters.yml \
   && cp parameters.php ./src/code/app/config/parameters.php \
   && cp custom.yml     ./src/code/app/config/custom.yml \
   && cp AppKernel.php ./src/code/app/AppKernel.php \
   && echo "installing dependencies" \
   && composer install --working-dir ./src/code --dev --no-interaction

